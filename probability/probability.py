import sys

sys.path.append('..')

from data_gen.Yu_data_gen import DataGenerator

class Probability:
    def __init__(self, universities = None, students = None):
        # load data
        if universities is not None:
            self.universities, self.students = universities, students
        else:
            self.universities, self.students = DataGenerator().get_data()

    def get_probability(self, person, university):
        '''
        @param person: a dict containing the basic information of a person, at least 'score'. For example {'score':700} 
        @param university: a dict containing information for a university.
        '''
        return 0.5