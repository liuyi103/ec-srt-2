#coding=utf-8
import sys
import operator

sys.path.append('..')

from data_gen.Yu_data_gen import DataGenerator
from probability.Xu_probability import Probability
from utility.utility import Utility

class Matching:
    @staticmethod
    def check_feasible(ans):
        '''
        check whether a solution is feasible
        :param ans: a matching solution
        :return: whether the solution is good.
        '''
        # TODO: implement it.
        return True

    def compatible(self, student, university): # check whether a matching is valid
        if ('wenli' in student) and ('wenli' in university) and (student['wenli'] != university['wenli']):
            return False
        if ('batch' in student) and ('batch' in university) and (student['batch'] != university['batch']):
            return False
        if ('year' in student) and ('year' in university) and (student['year'] != university['year']):
            return False
        if ('province' in student) and ('province' in university) and (student['province'] != university['province']):
            return False
        return True

    def __init__(self, universities = None, students = None):
        if universities is not None:
            self.universities, self.students = universities, students
        else:
            self.universities, self.students = DataGenerator().get_data()
        #print('Load Finish\n')
        self.prob_esti = Probability(self.universities, self.students)
        #print('Prob Finish\n')
        self.utility_calc = Utility()
        #print('Utility Finish\n')

    def get_results(self):
        m = 6    # number of slots
        ans = []
        for student in self.students:
            choices = []    # list of valid choices
            for university in self.universities:
                if self.compatible(student, university):
                    util = self.utility_calc.get_utility(student, university)
                    prob = self.prob_esti.get_probability(student, university)
                    if prob != '\\N':
                        choices.append({'util' : util, 'prob' : prob/100, 'sch' : university})
            #choices = [one for one in choices if one['prob'] > 0.5]
            choices.sort(key = operator.itemgetter('util'),reverse = True)    # sort in the descending order of utility
            n = len(choices)    # number of valid choices
            maxUtility=[[0 for j in range(m + 1)] for i in range(n + 1)]
            for i in range(n - 1, -1, -1):  # consider all choices after choice i
                for j in range(m + 1):      # number of remaining slots
                    maxUtility[i][j] = maxUtility[i + 1][j];    # ignore choice i
                    if j > 0:                                   # select choice i
                        maxUtility[i][j] = max(maxUtility[i][j],
                        choices[i]['util'] * choices[i]['prob'] + maxUtility[i + 1][j - 1] * (1 - choices[i]['prob']))
            slots = m
            recommendation = []
            for i in range(n):
                if (maxUtility[i][slots] != maxUtility[i+1][slots]) or (n - i <= slots):    # choose i
                    recommendation.append(choices[i])
                    slots -= 1
            ans.append(recommendation)
        return ans