import numpy as np
import sqlite3
import shlex #in order to recognize quotes
import random

SAMPLE_SCH = 200  # the number of users.
SAMPLE_STU = 500  # the number of users.


class DataGenerator_user:
    '''
    This is the base class for users' data generators.
    It is assumed that the user_info.db file already contains the preference information of several students.
    The information is as follows:
    1. name in 'name'
    2. habitat in 'habitat'
    3. score in 'score'.
    4. prefered cities in 'city1' ... 'city5'. Each city is specified by an integer.
    5. whether want to go abroad? Y=1, N=0, in 'abroad'.
    6. kinds of prefered school (medicine? teacher? sports? etc.) in 'kind1' ... 'kind5'. Each kind is specified by an integer. 
    7. whether want to take master/phd? in 'graduate'.
    8. whether care about gender? in 'gender', want more boy = 1, more girl = -1, no bias = 0.
    9. whether care about 985/211? in 'types'. Only can have 3 numbers: 985\211\0.
    Any data generator or probability estimator should have detailed document such that others can use it.
    '''
    def __init__(self):
        '''
        The constructor.
        '''
        import os
        conn = sqlite3.connect("./data_gen/user_info.db")
        cur = conn.cursor()
        cur.close()
        conn.commit()
        conn.close()

    def get_data(self):
        conn = sqlite3.connect("./data_gen/user_info.db")
        cur = conn.cursor()
        cur.execute("SELECT * FROM user")
        user = []
        for s in cur:
            user.append(
                {
                    'name' : s[0],
                    'score' : s[1],
                    'habitat': s[2]
                    'citie1' : s[3],
                    'citie2' : s[4],
                    'citie3' : s[5],
                    'citie4' : s[6],
                    'citie5' : s[7],
                    'abroad' : s[8],
                    'kind1' : s[9],
                    'kind2' : s[10],
                    'kind3' : s[11],
                    'kind4' : s[12],
                    'kind5' : s[13],
                    'graduate' : s[14],
                    "gender" : s[15],
                    "types" : s[16],
                }
            )
        return user