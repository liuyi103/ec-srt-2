#do not run this file, it is not up to date

'''
import:

	Download 'school.csv' and 'student.csv' from 'https://onedrive.live.com/redir?resid=343959E0FC2117DD!3704&authkey=!AIuYYhlfrYqOCWY&ithint=folder%2cmd'.
	Find 'importDatabase.py' from our git, put them in a the same file, and run 'importDatabase.py'.
	Or you can just run the following MySQL command:

		create database ec_srt
		use ec_srt
		create table school ( province varchar(20) NOT NULL, wenli int NOT NULL, year int NOT NULL, sch_name varchar(100) NOT NULL, major_name varchar(500) NOT NULL, batch int NOT NULL, plan_no int, enroll_no int, min_score int, max_score int, avg_score int);
		create table student ( batch int, province varchar(20) NOT NULL, rank int, sch_name varchar(100), score int, score_num int, major_name varchar(500), wenli int NOT NULL, year int NOT NULL);

		load data infile 'D:\\AtTsinghua\\2016Spring\\SRT\\school.csv' into table school lines terminated by '\r\n';
		load data infile 'D:\\AtTsinghua\\2016Spring\\SRT\\student.csv' into table student lines terminated by '\r\n';

		P.S. If you already have database ec_srt, you do not need to create it. If you already have table school and/or table student you have to drop them with 'drop table school;' and 'drop table student;'

		P.S. 'D:\\AtTsinghua\\2016Spring\\SRT\\school.csv' is just an example, you have to replace it with your file dir. Remember use '\\' instead of '\'.



export:
	select * from school into outfile 'D:\\AtTsinghua\\2016Spring\\SRT\\school.csv' lines terminated by '\r\n';
	select * from student into outfile 'D:\\AtTsinghua\\2016Spring\\SRT\\student.csv' lines terminated by '\r\n';

'''

import MySQLdb

conn = MySQLdb.connect(host='localhost', port=3306, user='root', passwd='123456')
cur = conn.cursor()
a = cur.execute("show databases")
if ('ec_srt',) not in cur.fetchmany(a):
    cur.execute("create database ec_srt")
cur.execute("use ec_srt")
a = cur.execute("show tables")
if ('school',) in cur.fetchmany(a):
    cur.execute("drop table school")
a = cur.execute("show tables")
if ('student',) in cur.fetchmany(a):
    cur.execute("drop table student")
cur.execute(
    "create table school ( province varchar(20) NOT NULL, wenli int NOT NULL, year int NOT NULL, sch_name varchar(100) NOT NULL, major_name varchar(500) NOT NULL, batch int NOT NULL, plan_no int, enroll_no int, min_score int, max_score int, avg_score int)")
cur.execute(
    "create table student ( batch int, province varchar(20) NOT NULL, rank int, sch_name varchar(100), score int, score_num int, major_name varchar(500), wenli int NOT NULL, year int NOT NULL)")

print 'loading school......'
cur.execute("load data infile 'D:\\\\AtTsinghua\\\\2016Spring\\\\SRT\\\\school.csv' into table school lines terminated by '\r\n'")

print 'loading student......'
cur.execute("load data infile 'D:\\\\AtTsinghua\\\\2016Spring\\\\SRT\\\\student.csv' into table student lines terminated by '\r\n'")

cur.close()
conn.commit()
conn.close()